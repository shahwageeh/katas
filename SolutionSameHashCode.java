package sameHashCode;

import java.util.Random;

public class SolutionSameHashCode {

	public static void generateStringsWithSameHashCode() {
		String stringA = generateRandomString();
		int h = stringA.hashCode();
		String stringB = generateStringWithSameHashCode(h, 31);
		String stringC = generateStringWithSameHashCode(h, 62);
		System.out.println("StringA = " + stringA);
		System.out.println("StringB = " + stringB);
		System.out.println("StringC = " + stringC);
		System.out.println(stringA.hashCode());
		System.out.println(stringB.hashCode());
		System.out.println(stringC.hashCode());

	}

	private static String generateStringWithSameHashCode(int h, int delta) {
		int H = h;
		StringBuilder stringBuilder = new StringBuilder();
		while (H > 0) {
			int ascii = 0;
			if (H < 127) {
				ascii = H;
			} else {
				ascii = H % 31 + delta;
			}
			stringBuilder.append((char) (ascii));
			H = (H - ascii) / 31;
		}
		return stringBuilder.reverse().toString();
	}

	private static String generateRandomString() {
		int leftLimit = 32;
		int rightLimit = 126;
		int targetStringLength = 5;
		Random random = new Random();
		StringBuilder stringBuilder = new StringBuilder(targetStringLength);
		for (int i = 0; i < targetStringLength; i++) {
			int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
			stringBuilder.append((char) randomLimitedInt);
		}
		return stringBuilder.toString();
	}

}
