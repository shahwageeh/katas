package stringCalculator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SolutionStringCalculator {

	//Step 1
//	public static int Add(String numbers) {
//		if (numbers.isEmpty()) {
//			return 0;
//		}
//		return Integer.parseInt(numbers.split(",")[0]) +
//				Integer.parseInt(numbers.split(",")[1]);
//	}
	
	//Step 2
//	public static int Add(String numbers) {
//		if (numbers.isEmpty()) {
//			return 0;
//		}
//		int sum = 0;
//		String[] stringArray = numbers.split(",");
//		for(int i = 0; i<stringArray.length; i++) {
//			sum+=Integer.parseInt(stringArray[i]);
//		}
//		return sum;
//	}
	
	//Step 3
//	public static int Add(String numbers) {
//		if (numbers.isEmpty()) {
//			return 0;
//		}
//		int sum = 0;
//		String simplifiedString = Matcher.quoteReplacement(numbers).replaceAll("\n", ",");
//		String[] stringArray = simplifiedString.split(",");
//		for(int i = 0; i<stringArray.length; i++) {
//			sum+=Integer.parseInt(stringArray[i]);
//		}
//		return sum;
//	}
	
	//Step 4
//	public static int Add(String numbers) {
//		if (numbers.isEmpty()) {
//			return 0;
//		}
//		int sum = 0;
//		String delimiter = numbers.substring(2, numbers.indexOf("\n"));
//		String simplifiedString = Matcher.quoteReplacement(numbers
//				.substring(numbers.indexOf("\n")+1))
//				.replaceAll("\n", delimiter);
//		String[] stringArray = simplifiedString.split(delimiter);
//		for(int i = 0; i<stringArray.length; i++) {
//			sum+=Integer.parseInt(stringArray[i]);
//		}
//		return sum;
//	}
	
	//Step 5
	public static int Add(String numbers) throws Exception{
		if (numbers.isEmpty()) {
			return 0;
		}
		if(numbers.contains("-")) {
			List<String> negatives = new ArrayList<>();
			Pattern pattern = Pattern.compile("\\-\\d*(\\.?\\d+)");
		    Matcher matcher = pattern.matcher(numbers);
		    while(matcher.find()) {
		    	negatives.add(matcher.group());
		    }
			throw new Exception("negatives not allowed :" + negatives.toString());
		}
		int sum = 0;
		String delimiter = numbers.substring(2, numbers.indexOf("\n"));
		String simplifiedString = Matcher.quoteReplacement(numbers
				.substring(numbers.indexOf("\n")+1))
				.replaceAll("\n", delimiter);
		String[] stringArray = simplifiedString.split(delimiter);
		for(int i = 0; i<stringArray.length; i++) {
			sum+=Integer.parseInt(stringArray[i]);
		}
		return sum;
	}
}
